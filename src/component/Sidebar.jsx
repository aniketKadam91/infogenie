import React, { Component } from 'react'
import '../assets/css/Sidebar.css'

export class Sidebar extends Component {
    render() {
        return (
            <div>
                <div className="sidebar">
                    <div className="sidebar-records">
                        <ul>
                            <li> Company Profile</li>
                            <li className="active"> Contacts</li>
                        </ul>
                    </div>
                    <div className="sidebar-search">
                    <form className="form-inline my-5 my-lg-0">
                        <input className="form-control mr-sm-8" type="search" placeholder="Search" aria-label="Search" />
                        <span className="searchicon"><i class="fa fa-search" aria-hidden="true"></i></span>
                       
                    </form>
                    </div>
                    <div className="sidebar-conatct">
                        <ul>
                        <li>
                                <div className="profile-icon">
                                    <span className="user-ico"><i class="fa fa-user-circle" aria-hidden="true"></i></span>
                                </div>
                                <div className="profile-info">
                                    <h6>Jaswinder Singh</h6>
                                    <div><span className="pro-ico"><i class="fa fa-suitcase" aria-hidden="true"></i></span>Senior Software Analyst</div>
                                    <div><span className="pro-ico"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>jaswinder@gamil.com</div>
                                    <div><span className="pro-ico"><i class="fa fa-phone" aria-hidden="true"></i></span>+91 7766554411</div>
                                </div>
                                <div className="linkedin-icon"><i class="fa fa-linkedin"></i></div>
                            </li>
                            <li>
                                <div className="profile-icon">
                                    <span className="user-ico"><i class="fa fa-user-circle" aria-hidden="true"></i></span>
                                </div>
                                <div className="profile-info">
                                    <h6>Jaswinder Singh</h6>
                                    <div><span className="pro-ico"><i class="fa fa-suitcase" aria-hidden="true"></i></span>Senior Software Analyst</div>
                                    <div><span className="pro-ico"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>jaswinder@gamil.com</div>
                                    <div><span className="pro-ico"><i class="fa fa-phone" aria-hidden="true"></i></span>+91 7766554411</div>
                                </div>
                                <div className="linkedin-icon"><i class="fa fa-linkedin"></i></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default Sidebar

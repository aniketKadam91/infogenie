import React, { Component } from 'react'
import '../assets/css/Home.css'
import Data from '../data.json'


export class Home extends Component {
    constructor(props){
        super(props);
    
        this.state={
          search:null
        };
      }

    render() {
        function countObjectKeys(obj) { 
            return Object.keys(obj).length; 
        }
        return (
            <div>
                
                    <div className="main-wrapper">
                        <div className="comany-records">
                            <ul>
                                <li><span className="building-icon"><i className="fa fa-building-o" aria-hidden="true"></i></span><span className="records-no">{countObjectKeys(Data)}</span> Companies</li>
                                <li><span className="group-icon"><i className="fa fa-group" aria-hidden="true"></i></span><span className="records-no">4,029,312</span> Contacts Found</li>
                            </ul>
                        </div>
                        
                        <div className="company-list active">
                            <ul className="top-ul">
                            {
                                Data.filter((post)=>{
                                    if(this.props.searchkey == null)
                                        return post
                                    else if(post.company_name.toLowerCase().includes(this.props.searchkey.toLowerCase()) || post.website.toLowerCase().includes(this.props.searchkey.toLowerCase())){
                                        return post
                                    }
                                  }).map((post,index)=>{
                                    return(
                                    <li className="comapany-lists">
                                        <div className="first element">
                                    <h4>{post.company_name}</h4>
                                            <div className="bottom-list">
                                                <ul>
                                    <li className="country-name">{post.country}</li>
                                                    <li className="website-name">{post.website}</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="second element">
                                            <div><span className="group-icon"><i className="fa fa-user-o" aria-hidden="true"></i></span>{post.emplyee_strength}</div>
                                            <div><span className="group-icon"><i className="fa fa-building-o" aria-hidden="true"></i></span>{post.industry}</div>
                                        </div>
                                        <div className="third element">
                                            <div><span className="group-icon">$</span>{post.reveinue}</div>
                                            <div><span className="group-icon"><i className="fa fa-building-o" aria-hidden="true"></i></span>{post.company_type}</div>
                                        </div>
                                    </li>
                                    )
                                })
                            }

                            </ul>
                        </div>
                    </div>
                </div>
            
        )
    }
}

export default Home

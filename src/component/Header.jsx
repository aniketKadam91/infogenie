import React, { Component } from 'react'
import logo1 from '../assets/images/infogenie.png';
import '../assets/css/Header.css'
import Popup from './Popup'; 

export class Header extends Component {

  constructor(props){
    super(props);

    this.state={
      search:null,
      showPopup: false
    };
  }

  togglePopup() { 
    this.setState({  
        showPopup: !this.state.showPopup 
    });  
    //console.log(this.state.id);
  }

  searchSpace=(event)=>{
    let keyword = event.target.value;
    //this.setState({search:keyword})
    this.props.search(keyword);
  }
    render() {
        return (
            <div>
                <header>
                    <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light">
        <a className="navbar-brand" href="#"><img src={logo1} className="site-logo" alt="logo" /></a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
            <form className="form-inline my-5 my-lg-0 search-form">
            <input className="form-control mr-sm-8" type="search" placeholder="Search Companies by Name, URL" aria-label="Search" onChange={(e)=>this.searchSpace(e)} />
            <span className="searchicon"><i className="fa fa-search" aria-hidden="true"></i></span>
            
          </form>
            </li> 
            <li className="nav-item">
              <a className="nav-link search-option"onClick={this.togglePopup.bind(this)}>Advance Search<span><i className="fa fa-angle-down" aria-hidden="true"></i></span></a>
            </li> 
            <li className="nav-item">
              <a className="nav-link downoad-btn" href="#"><span className="download-icon"><i className="fa fa-download" aria-hidden="true"></i></span>Download All </a>
            </li>      

          </ul>

        </div>
      </nav>
      </div>
                </header>


                {this.state.showPopup ?  
                    <Popup  
                              
                            closePopup={this.togglePopup.bind(this)}  
                    />  
                    : null  
                    }  
                 
            </div>
            
        )
    }
}

export default Header

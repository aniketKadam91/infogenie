import React from 'react';  
import '../assets/css/Popup.css';

class Popup extends React.Component {  

    
  render() {  
    

    return (  
            <div className='popup'>  
                <div className='popup_inner'> 
                    <div className="popup-header">
                        Advanced Search
                        <button className="close-icon" onClick={this.props.closePopup}>X</button>  
                    </div>
                    <div className="popup-body">
                        <div className="row">
                            <div className="w3-sidebar w3-bar-block col-md-2">
                                <a href="#" className="w3-bar-item w3-button">Comapny Filter</a>
                                <a href="#" className="w3-bar-item w3-button">Employee size | Revenue</a>
                                <a href="#" className="w3-bar-item w3-button">Region</a>
                                <a href="#" className="w3-bar-item w3-button">Industry</a>
                                <a href="#" className="w3-bar-item w3-button">Tech Install</a>
                                <a href="#" className="w3-bar-item w3-button">Other Criteria</a>
                                <a href="#" className="w3-bar-item w3-button">Contact Filter</a>
                                <a href="#" className="w3-bar-item w3-button">Level</a>
                                <a href="#" className="w3-bar-item w3-button">Function</a>
                                <a href="#" className="w3-bar-item w3-button">Information Avaliability</a>
                            </div> 
                            <div className="filter-body col-md-5">
                                <h6>Employee Size</h6>
                                <ul>
                                    <li><input type="checkbox" name="emp_size" value="all"/>All</li>
                                    <li><input type="checkbox" name="emp_size" value="51"/>51 - 200</li>
                                    <li><input type="checkbox" name="emp_size" value="201"/>201 - 500</li>
                                    <li><input type="checkbox" name="emp_size" value="501"/>501 - 1000</li>
                                    <li><input type="checkbox" name="emp_size" value="1001"/>1001 - 5000</li>
                                    <li><input type="checkbox" name="emp_size" value="5001"/>5001 - 10,000</li>
                                    <li><input type="checkbox" name="emp_size" value="10000"/>10,000+</li>
                                    <li><input type="checkbox" name="emp_size" value="unavilable"/>Unavialable</li>
                                </ul>
                            </div>
                            <div className="col-md-5 filter-body">
                                <h6>Revenue</h6>
                                <ul>
                                    <li><input type="checkbox" name="revinue_size" value="all"/>All</li>
                                    <li><input type="checkbox" name="revinue_size" value="0"/>$0 - 1M</li>
                                    <li><input type="checkbox" name="revinue_size" value="1"/>$1 - 10M</li>
                                    <li><input type="checkbox" name="revinue_size" value="10"/>$10 - 50M</li>
                                    <li><input type="checkbox" name="revinue_size" value="50"/>$50 - 100M</li>
                                    <li><input type="checkbox" name="revinue_size" value="100"/>$100 -  250M</li>
                                    <li><input type="checkbox" name="revinue_size" value="250"/>$250 - 500M</li>
                                    <li><input type="checkbox" name="revinue_size" value="500"/>$500 - 1B</li>
                                    <li><input type="checkbox" name="revinue_size" value="1B"/>$1B+</li>
                                    <li><input type="checkbox" name="revinue_size" value="not_avb"/>Unavialable</li>
                                </ul>
                            </div>
                        </div>


                    </div>
                    <div className="popup-footer">
                        <div className="footer-button">
                            <button className="button">Canecel</button>
                            <button className="button">Apply</button>
                        </div>

                    </div>
                </div>  
            </div>  
        );  
    }  
}  

export default Popup;
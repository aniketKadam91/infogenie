import React from 'react';
import './App.css';
import Header from './component/Header'
import 'bootstrap/dist/css/bootstrap.css';
import Home from './component/Home'
import Sidebar from './component/Sidebar'

import  { Component } from 'react'

export class App extends Component {

  constructor(){
    super();

    this.state={
      search:null
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(ser){
    this.setState({
      search:ser
    })
    //console.log(this.state.search);
  }

  render() {
    return (
      <div className="App">
      <Header search={this.handleChange}></Header>
      <div className="container">
      <Home searchkey={this.state.search}></Home>
      <Sidebar></Sidebar>
      </div>
      </div>
    )
  }
}

export default App


